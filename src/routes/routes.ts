import { Application } from 'express'
import { inject, injectable } from 'inversify'
import { Controller } from '../controller/controller'

@injectable()
export class Routes{
    public controller: Controller
    constructor( @inject(Controller) controller: Controller){
        this.controller = controller
    }
    

    public routes(app: Application):void {
        app.get('/get',this.controller.getEntry)
        app.post('/create',this.controller.createEntry)
    }

}