import 'reflect-metadata'
import diContainer from './config/di-container'
import { Routes } from './routes/routes'
import express,{Application} from 'express'
import mongoose, { connection } from 'mongoose'
import { injectable, inject } from 'inversify'

//const port = 7070
const mongoUrl = 'mongodb://localhost:27017/tsDB'
const routes: Routes = diContainer.resolve<Routes>(Routes)
const serverPort = 7070


export class App{

    public app: Application 
    public routes: Routes
    constructor(routes: Routes){
        this.app = express()
        this.routes = routes
        this.intialize()
        this.routes.routes(this.app)
    }

    public intialize(){
        this.app.use(express.json())
        this.mongosetup()
    }

    public mongosetup(){
        mongoose.connect(mongoUrl)
        mongoose.connection.on('connected', () => {
            console.log('DB Connected')
        })
    }
}

const appObj: App = new App(routes)

appObj.app.listen(serverPort, () => {
    console.log("Server running on port ", serverPort)
})




