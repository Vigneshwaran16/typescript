import { inject, injectable } from 'inversify'
import { Dao } from '../dao/dao'
import { MovieDbModel } from '../model/model';
import {Request } from 'express'

@injectable()
export class Service{
    public dao: Dao

    constructor(@inject(Dao) dao: Dao){
        this.dao = dao
    }
    public async getEntry(){
        return await this.dao.getEntry()
    }

    public async createEntry(body: MovieDbModel){
        return await this.dao.createEntry(body)
    }

    public async deleteEntry(req: Request){
        return await this.dao.deleteEntry(req.params.id)
    }

    public async getById(req: Request){
        return await this.dao.getById(req.params.id)
    }

    public async updateEntry(req: Request){
        return await this.dao.updateEntry(req.params.id, req.body)

    }



}