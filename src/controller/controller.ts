import {Service} from '../service/service'
import { injectable, inject } from 'inversify'
import { MovieDbModel, modelSchema } from '../model/model'
import {Request , Response} from 'express'

@injectable()
export class Controller{
    public service:Service

    constructor(@inject(Service) services: Service){
        this.service = services
    }
    
    async getEntry(req: Request, res: Response){        
        try{
            const entries: MovieDbModel[] = await this.service.getEntry()
            if(entries == null || entries == []){
                throw "Empty Database"
            }
            else{
                res.status(200).json({movies: entries})
            }
        }
        catch(error){
            res.status(400).json({err: error})
        }
        //res.send(name)
    }

    async createEntry(req : Request, res: Response){
        try{
            const movieEntry = await this.service.createEntry(req.body)
            if(movieEntry == null  || movieEntry == undefined){
                throw "Cannot create Entry"
            }
            else{
                res.status(200).json({created_entry: movieEntry})
            }
         }
         catch(error){
            if(error.name == "ValidationError"){
                res.status(400).json({error: "Please add fields as per Schema"})
            }
            else{
                res.status(500).json({error : error})
            }
         }

    }

    public async deleteEntry(req: Request, res: Response){
        try{
            const movieEntry: MovieDbModel = await this.service.deleteEntry(req)
            if(movieEntry == null || movieEntry == undefined){
                throw "Cannot delete Entry"
            }
            else{
                res.status(200).json({deleted: movieEntry})
            }
        }
        catch(error){
            res.status(400).json({error: error})
        }

    }

    
    public async getById(req: Request, res: Response){
        try{
            const movieEntry: MovieDbModel = await this.service.getById(req)
            if(movieEntry == null || movieEntry == undefined){
                throw "Cannot find the movie with this id"
            }
            else{
                res.status(200).json({movie: movieEntry})
            }
        }
        catch(error){
            res.status(400).json({error: error})
        }
        
    }

    
    public async updateEntry(req: Request, res: Response){
        try{
            const movieEntry: MovieDbModel = await this.service.updateEntry(req)
            if(movieEntry == null || movieEntry == undefined){
                throw "Cannot update Entry"
            }
            else{
                res.status(200).json({updated: movieEntry})
            }
        }
        catch(error){
            res.status(400).json({error: error})
        }
        
    }

}