import { Container } from 'inversify'
import { Controller } from '../controller/controller'
import { Service } from '../service/service'
import {Routes} from '../routes/routes'
import {Dao} from '../dao/dao'
import  {App} from '../app'

const diContainer: Container = new Container()

diContainer.bind<Routes>(Routes).toSelf()
diContainer.bind<Controller>(Controller).toSelf()
diContainer.bind<Service>(Service).toSelf()
diContainer.bind<Dao>(Dao).toSelf()


export default diContainer
